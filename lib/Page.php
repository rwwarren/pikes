<?php

abstract class Page {

    final public function render() {
      date_default_timezone_set('America/Los_Angeles');
      $content =
        '<!DOCTYPE html>' .
        '<html>' .
        '<head>' .
        '<link href="/css/styles.css" rel="stylesheet" type="text/css" />' .
        '<title>' . $this->getTitle() . '</title>' .
        '<link rel="icon" href="/css/favicon.ico" type="image/x-icon" />' .
        '</head>' .
        '<body>' .
        '<div id="contains">' .
        '<div id="header">' .
        $this->getHeader() .
        '</div>' .

        '<div id="wrapper">' .
        $this->nav() .
        '<div id="content">' .
        $this->getContent() .
        '</div>' .
        '</div>' .

        '<div id="footer">' .
        $this->getFooter() .
        '</div>' .
        '</div>' .
        '</body>' .
        '</html>';
      echo $content;
    }

    public function getTitle(){
      return 'UW Pikes';
    }

    public function getHeader(){
      return '';
    }

    public function getFooter() {
      return
        '<ul>' .
        '<li><a href="http://depts.washington.edu/greek/ifc/">UW IFC Site</a></li>' .
        ' <li><a href="http://depts.washington.edu/greek/ifc/chapters/pi-kappa-alpha">' .
        'Pikes UW IFC Site</a></li>' .
        ' <li><a href="http://www.pikes.org">Pikes National Site</a></li>' .
        '</ul>' .
        '<p>&copy ' . date("Y") . ' UW Pikes';
    }

    public function nav() {
      return
        '<div id="nav">' .
        '<p><a href="/">Index</a>' .
        '<p><a href="/about">About</a>' .
        '<p><a href="/location">Location</a>' .
        '<p><a href="/gallery">Gallery</a>' .
        '<p><a href="/rush">Rush</a>' .
        '<p><a href="/contact">Contact Us</a>' .

        '</div>' .
        '<br>';//TODO edit this shit
    }

    abstract function getContent();

}

?>

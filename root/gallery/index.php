<?php
require_once('../../lib/Page.php');

class gallery extends Page {

  public function getTitle() {
    return 'UW Pikes Gallery';
  }

  public function getContent() {
    return
      'Gallery' .
      '<p>Make a JQuery photo viewer';
  }

}
$page = new gallery();
$page->render();
?>

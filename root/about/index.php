<?php
require_once('../../lib/Page.php');

class About extends Page {

  public function getTitle() {
    return 'About Pikes at UW';
  }

  public function getContent() {
    return
      'About us';
  }

}
$page = new About();
$page->render();
?>
